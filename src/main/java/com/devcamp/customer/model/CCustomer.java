package com.devcamp.customer.model;

import javax.persistence.*;

@Entity
@Table(name="customers")
public class CCustomer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name="ho_ten")
	private String hoTen;
	
	@Column(name="email")
	private String Email;
	
	@Column(name="so_dien_thoai")
	private String soDienThoai;
	
	@Column(name="dia_chi")
	private String diaChi;
	
	@Column(name="ngay_tao")
	private long ngayTao;
	
	@Column(name="ngay_cap_nhat")
	private long ngayCapNhat;

	public CCustomer(long id, String hoTen, String email, String soDienThoai, String diaChi, long ngayTao,
			long ngayCapNhat) {

		this.id = id;
		this.hoTen = hoTen;
		Email = email;
		this.soDienThoai = soDienThoai;
		this.diaChi = diaChi;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
	}

	public CCustomer() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getHoTen() {
		return hoTen;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getSoDienThoai() {
		return soDienThoai;
	}

	public void setSoDienThoai(String soDienThoai) {
		this.soDienThoai = soDienThoai;
	}

	public String getDiaChi() {
		return diaChi;
	}

	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}

	public long getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(long ngayTao) {
		this.ngayTao = ngayTao;
	}

	public long getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(long ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

}
