package com.devcamp.customer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customer.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {

}
