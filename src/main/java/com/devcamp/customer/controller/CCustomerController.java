package com.devcamp.customer.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customer.model.CCustomer;
import com.devcamp.customer.repository.ICustomerRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CCustomerController {
	@Autowired
	ICustomerRepository pCustomerRepository;

	@GetMapping("/customers")
	public ResponseEntity<List<CCustomer>> getAllCustomer() {
		try {
			List<CCustomer> pCustomers = new ArrayList<CCustomer>();
			pCustomerRepository.findAll().forEach(pCustomers::add);
			return new ResponseEntity<>(pCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
